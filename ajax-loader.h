/*

  A simple widget to show a load indicator animation in the middle of parent widget

  Gif made with http://www.ajaxload.info/ under DWTFYWTPL (damn free license, check the site)

*/

#pragma once

#include <QLabel>
#include <QMovie>

class AjaxLoader: public QLabel
{
public:
    AjaxLoader(QWidget* parent);
    ~AjaxLoader();

public Q_SLOTS:
    void show();
    void hide();

private:
    QMovie *m_movie;
};
