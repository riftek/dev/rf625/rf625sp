#pragma once

#include <QObject>
#include "rf625.h"

class SensorsWorker: public QObject
{
Q_OBJECT

public:
    SensorsWorker(QObject *parent = Q_NULLPTR);
    virtual ~SensorsWorker();
    inline void setSource(std::shared_ptr<rf625> dev) {m_dev = dev;}
    void stop();

public Q_SLOTS:
    void run();

Q_SIGNALS:
    void updated(void*);
    void finished();

private:
    std::shared_ptr<rf625> m_dev;
    rf625_sensors *m_sensors;
};
