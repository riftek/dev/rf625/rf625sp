#include "common.h"
#include <QMessageBox>
#include <QTimer>
#include <QThread>
#include <QFileDialog>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "xconfig.h"
#include "search.h"
#include "image.h"
#include "profile.h"
#include "upgrade.h"
#include "sensors.h"
#include "idle.h"
#include "ajax-loader.h"

XConfig *params = Q_NULLPTR;
SearchWorker *searchWorker = Q_NULLPTR;
ImageWorker *imageWorker = Q_NULLPTR;
ProfileWorker *profileWorker = Q_NULLPTR;
UpgradeWorker *upgradeWorker = Q_NULLPTR;
SensorsWorker *sensorsWorker = Q_NULLPTR;
IdleWorker *idleWorker = Q_NULLPTR;
AjaxLoader *loader = Q_NULLPTR;

enum LeftTabIndeces {
    Tab_Search,
    Tab_Params,
    Tab_Service
};
enum RightTabIndeces {
    Tab_Info,
    Tab_Image,
    Tab_Profile,
    Tab_Sensors
};
int prevTab;

int workMode = Mode_Idle;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_initState(false),
    m_dev(Q_NULLPTR)
{
    qApp->setApplicationName(APP_NAME);
    qApp->setApplicationDisplayName(APP_DISPLAY_NAME);
    qApp->setOrganizationName(ORGANIZATION_NAME);
    qApp->setOrganizationDomain(ORGANIZATION_DOMAIN);
    qApp->setApplicationVersion(QString("%1.%2.%3.%4")
        .arg(static_cast<int>((APP_VERSION>>16)&0xff))
        .arg(static_cast<int>((APP_VERSION>>8)&0xff))
        .arg(static_cast<int>((APP_VERSION)&0xff))
        .arg(APP_REVISION)
    );
    setWindowTitle(qApp->applicationDisplayName());
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete params;
}

void MainWindow::onCreate()
{
    /* initialize librf625 */
    rf625::init();

    /* Setup found devices table widget */
    ui->tabWidget_Left->setFixedWidth(360);
    ui->tableWidget_Devices->verticalHeader()->setVisible(false);
    ui->tableWidget_Devices->setSortingEnabled(false);
    ui->tableWidget_Devices->insertColumn(0);
    QTableWidgetItem *item = new QTableWidgetItem(tr("IP Address"));
    ui->tableWidget_Devices->setHorizontalHeaderItem(0, item);
    ui->tableWidget_Devices->setColumnWidth(0, 80);
    ui->tableWidget_Devices->insertColumn(1);
    item = new QTableWidgetItem(tr("Serial"));
    ui->tableWidget_Devices->setHorizontalHeaderItem(1, item);
    ui->tableWidget_Devices->setColumnWidth(1, 50);
    ui->tableWidget_Devices->insertColumn(2);
    item = new QTableWidgetItem(tr("MAC Address"));
    ui->tableWidget_Devices->setHorizontalHeaderItem(2, item);
    ui->tableWidget_Devices->setColumnWidth(2, 100);
    ui->tableWidget_Devices->insertColumn(3);
    item = new QTableWidgetItem(tr("Version"));
    ui->tableWidget_Devices->setHorizontalHeaderItem(3, item);
    ui->tableWidget_Devices->horizontalHeader()->setStretchLastSection(true);
    ui->tableWidget_Devices->setSelectionBehavior(QTableWidget::SelectRows);

    /* Setup parameters table widget */
    ui->tableWidget_Parameters->verticalHeader()->setVisible(false);
    ui->tableWidget_Parameters->setSortingEnabled(false);
    ui->tableWidget_Parameters->insertColumn(0);
    item = new QTableWidgetItem(tr("Parameter"));
    ui->tableWidget_Parameters->setHorizontalHeaderItem(0, item);
    ui->tableWidget_Parameters->setColumnWidth(0, 180);
    ui->tableWidget_Parameters->insertColumn(1);
    item = new QTableWidgetItem(tr("Value"));
    ui->tableWidget_Parameters->setHorizontalHeaderItem(1, item);
    ui->tableWidget_Parameters->horizontalHeader()->setStretchLastSection(true);
    ui->tableWidget_Parameters->setSelectionBehavior(QTableWidget::SelectRows);

    params = new XConfig(Q_NULLPTR, QString(), ui->tableWidget_Parameters);

    QStringList selectVals;

    params->AddSpacer(tr("Network"));
    params->AddEntry(IPAddress, "ip_address", XConfig::MakeIPAddress_t(0,0,0,0), tr("IP Address"));
    params->AddEntry(IPAddress, "subnet_mask", XConfig::MakeIPAddress_t(0,0,0,0), tr("Subnet Mask"));
    params->AddEntry(Int, "tcp_port", 0, tr("TCP Port"), QString(), QPointF(1,65535));
    params->AddEntry(Bool, "tcp_autodisconnect_enable", false, tr("TCP Autodisconnect Enable"));
    params->AddEntry(Int, "tcp_autodisconnect_timeout", 0, tr("TCP Autodisconnect Timeout"), QString(), QPointF(0,65535));
    params->AddEntry(IPAddress, "udp_host_address", XConfig::MakeIPAddress_t(0,0,0,0), tr("UDP Host Address"));
    params->AddEntry(Int, "udp_port", 0, tr("UDP Port"), QString(), QPointF(1,65535));
    params->AddEntry(Bool, "udp_stream_enable", false, tr("UDP Stream Enable"));

    params->AddSpacer(tr("General"));
    params->AddEntry(Bool, "laser_enable", false, tr("Laser Enable"));
    params->AddEntry(Int, "laser_level", 0, tr("Laser Level"), QString(), QPointF(0,255));
    params->AddEntry(Int, "exposure_time", 0, tr("Exposure Time, us"), QString(), QPointF(0,3824));
    selectVals << "80" << "160" << "320" << "640" << "1280";
    params->AddEntry(Select, "resolution", selectVals, tr("Resolution"));
    params->AddEntry(Int, "pixel_brightness_thres", 0, tr("Brightness Filter Thres."), QString(), QPointF(0,255));
    params->AddEntry(Int, "diff_brightness_thres", 0, tr("Diff Filter Thres."), QString(), QPointF(0,255));
    params->AddEntry(Bool, "dhs_enable", false, "DHS Enable");
    params->AddEntry(Bool, "raw_image", false, tr("Raw Image"));
    params->AddEntry(Bool, "invert_by_x", false, tr("Invert by X"));
    params->AddEntry(Bool, "invert_by_z", false, tr("Invert by Z"));
    params->AddEntry(Bool, "fixed_frame_enable", false, tr("Fixed Frame Enable"));

    params->AddSpacer(tr("Bracketing"));
    params->AddEntry(Bool, "bracketing_enable", false, tr("Bracketing Enable"));
    params->AddEntry(Int, "bracketing_exposure_0", 0, tr("Bracketing Exposure 0"), QString(), QPointF(0,3824));
    params->AddEntry(Int, "bracketing_exposure_1", 0, tr("Bracketing Exposure 1"), QString(), QPointF(0,3824));

    params->AddSpacer(tr("Trigger"));
    selectVals.clear();
    selectVals << "Max Frequency" << "Custom Frequency" << "External Input" << "Encoder" << "Step/Dir";
    params->AddEntry(Select, "trigger", selectVals, tr("Trigger"));
    params->AddEntry(Int, "udp_frequency", 0, tr("UDP Frequency"), QString(), QPointF(1,500));
    params->AddEntry(Int, "ext_sync_divisor", 0, tr("Ext. Sync Divisor"), QString(), QPointF(0,65535));
    selectVals.clear();
    selectVals << "None" << "1" << "2" << "3" << "4";
    params->AddEntry(Select, "sync_input_channel", selectVals, tr("Sync Input Channel"));
    params->AddEntry(Bool, "encoder_has_zero", false, tr("Encoder has Zero"));

    params->AddSpacer(tr("Sync"));
    params->AddEntry(Bool, "measure_sync_enable", false, tr("Measure Sync Enable"));
    params->AddEntry(Select, "measurement_sync_input_channel", selectVals, tr("Measurement Sync Input Channel"));
    params->AddEntry(Int, "measurement_sync_delay", 0, tr("Measurement Sync Delay, us"), QString(), QPointF(0,65535));
    selectVals.removeLast();
    selectVals.removeLast();
    params->AddEntry(Select, "output_channel", selectVals, tr("Output Channel"));

    params->AddSpacer(tr("ROI"));
    params->AddEntry(Bool, "roi_enable", false, "ROI Enable");
    selectVals.clear();
    for (int i=0; i<480; i+=32)
    {
        selectVals << QString::number(i);
    }
    params->AddEntry(Select, "roi_top", selectVals, tr("Window Top"));
    selectVals << QString::number(480);
    selectVals.removeFirst();
    params->AddEntry(Select, "roi_height", selectVals, tr("Window Height"));

    /* setup ui... */
    ui->pushButton_Connect->setEnabled(false);
    ui->pushButton_Stream->setEnabled(false);
    ui->tableWidget_Parameters->setEnabled(false);
    ui->pushButton_Apply->setEnabled(false);
    ui->pushButton_Save->setEnabled(false);
    ui->pushButton_RestoreFactory->setEnabled(false);

    ui->tabWidget_Left->setTabEnabled(Tab_Params, false);
    ui->tabWidget_Left->setTabEnabled(Tab_Service, false);
    ui->tabWidget_Right->setTabEnabled(Tab_Image, false);
    ui->tabWidget_Right->setTabEnabled(Tab_Profile, false);
    ui->tabWidget_Right->setTabEnabled(Tab_Sensors, false);

    ui->textBrowser_Info->setOpenExternalLinks(true);

    ui->label_Reboot->setText(tr("<a href=\":reboot\">Reboot</a>"));
    ui->label_PowerOff->setText(tr("<a href=\":poweroff\">Power Off</a>"));
    ui->label_Upgrade->setText(tr("<a href=\":upgrade\">Upgrade Firmware</a>"));

    /* connections */
    connect(ui->pushButton_Search, SIGNAL(clicked()), this, SLOT(clickedSearch()));
    connect(ui->pushButton_Connect, SIGNAL(clicked()), this, SLOT(clickedConnect()));
    connect(ui->tableWidget_Devices, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(clickedConnect()));
    connect(ui->pushButton_Stream, SIGNAL(clicked()), this, SLOT(clickedStream()));
    connect(ui->pushButton_Apply, SIGNAL(clicked()), this, SLOT(clickedApply()));
    connect(ui->pushButton_Save, SIGNAL(clicked()), this, SLOT(clickedSave()));
    connect(ui->pushButton_RestoreFactory, SIGNAL(clicked()), this, SLOT(clickedRestoreFactory()));
    connect(ui->tabWidget_Right, SIGNAL(currentChanged(int)), this, SLOT(tabChanged(int)));
    connect(ui->customPlot, SIGNAL(frameRateUpdated(int)), this, SLOT(frameRateUpdated(int)));
    connect(ui->customPlot, SIGNAL(profileUpdated(void*)), this, SLOT(profileUpdated(void*)));
    connect(ui->tableWidget_Devices, SIGNAL(itemSelectionChanged()), this, SLOT(devListSelChanged()));
    connect(this, SIGNAL(updateProfile(void*)), ui->customPlot, SLOT(setProfile(void*)));
    connect(ui->comboBox_ImageSize, SIGNAL(currentIndexChanged(int)), this, SLOT(imageSizeChanged(int)));
    connect(ui->pushButton_SaveImage, SIGNAL(clicked()), this, SLOT(clickedSaveImage()));
    connect(ui->label_Reboot, SIGNAL(linkActivated(QString)), this, SLOT(linkActivated(QString)));
    connect(ui->label_PowerOff, SIGNAL(linkActivated(QString)), this, SLOT(linkActivated(QString)));
    connect(ui->label_Upgrade, SIGNAL(linkActivated(QString)), this, SLOT(linkActivated(QString)));
    connect(ui->customPlot, SIGNAL(alternateMouseDoubleClickEvent(QMouseEvent*)), this, SLOT(doubleClickedPlot(QMouseEvent*)));

    ui->comboBox_ImageSize->setCurrentIndex(0);

    loader = new AjaxLoader(this->centralWidget());

    QTimer::singleShot(0, this, SLOT(populateInfo()));
}

void MainWindow::onClose()
{
    disconnect();

    if (m_dev) {
        deviceDisconnect();
    }

    /* cleanup */
    rf625::cleanup();

    delete loader;
}

void MainWindow::clickedSearch()
{
    if (searchWorker) {
        return;
    }

    if (m_dev) {
        deviceDisconnect();
    }

    while (ui->tableWidget_Devices->rowCount()) {
        ui->tableWidget_Devices->removeRow(ui->tableWidget_Devices->rowCount()-1);
    }
    ui->pushButton_Search->setEnabled(false);
    ui->pushButton_Connect->setEnabled(false);
    qApp->processEvents();

    QTimer::singleShot(0, this, SLOT(populateInfo()));

    QTimer::singleShot(0, this, SLOT(search()));
}

void MainWindow::clickedConnect()
{
    if (m_dev)
    {
        deviceDisconnect();
        ui->pushButton_Stream->setEnabled(true);
        ui->pushButton_Search->setEnabled(true);
        return;
    }

    int row = ui->tableWidget_Devices->currentRow();
    if (row < 0)
        return;

    m_dev = m_devList.at(row);

    if (!m_dev->connect()) {
        QMessageBox::critical(this, tr("Fatal"), tr("Failed to connect"));
        return;
    }

    ui->tableWidget_Devices->setEnabled(false);
    ui->pushButton_Connect->setText(tr("Disconnect"));
    ui->pushButton_Search->setEnabled(false);
    ui->pushButton_Stream->setEnabled(false);

    workMode = Mode_TCP;

    /* read parameters */
    if (!m_dev->read_params(m_devParams)) {
        QMessageBox::critical(this, tr("Fatal"), tr("Failed to read device parameters"));
        return;
    }

    QTimer::singleShot(0, this, SLOT(populateParams()));

    ui->tabWidget_Left->setTabEnabled(Tab_Params, true);
    ui->tabWidget_Left->setTabEnabled(Tab_Service, true);
    ui->tableWidget_Parameters->setEnabled(true);
    ui->pushButton_Apply->setEnabled(true);
    ui->pushButton_Save->setEnabled(true);
    ui->pushButton_RestoreFactory->setEnabled(true);
    ui->tabWidget_Right->setTabEnabled(Tab_Image, true);
    ui->tabWidget_Right->setTabEnabled(Tab_Profile, true);
    ui->tabWidget_Right->setTabEnabled(Tab_Sensors, true);

    ui->tabWidget_Left->setCurrentIndex(Tab_Params);
    prevTab = -1;
    tabChanged(Tab_Info);   // start idle worker
}

void MainWindow::clickedStream()
{
    if (m_dev)
    {
        deviceDisconnect();
        ui->tabWidget_Right->setCurrentIndex(Tab_Info);
        ui->pushButton_Stream->setText(tr("Stream"));
        ui->pushButton_Connect->setEnabled(true);
        ui->pushButton_Search->setEnabled(true);
        return;
    }

    int row = ui->tableWidget_Devices->currentRow();
    if (row < 0)
        return;

    m_dev = m_devList.at(row);

    ui->tableWidget_Devices->setEnabled(false);
    ui->pushButton_Connect->setEnabled(false);
    ui->pushButton_Search->setEnabled(false);
    ui->pushButton_Stream->setText(tr("Stop"));

    workMode = Mode_UDP;

    QTimer::singleShot(0, this, SLOT(startStream()));
}

void MainWindow::clickedApply()
{
    ui->pushButton_Apply->setEnabled(false);
    ui->pushButton_Save->setEnabled(false);
    ui->pushButton_RestoreFactory->setEnabled(false);

    applyParams();

    ui->pushButton_Apply->setEnabled(true);
    ui->pushButton_Save->setEnabled(true);
    ui->pushButton_RestoreFactory->setEnabled(true);
}

void MainWindow::clickedSave()
{
    ui->pushButton_Apply->setEnabled(false);
    ui->pushButton_Save->setEnabled(false);
    ui->pushButton_RestoreFactory->setEnabled(false);

    saveParams();

    ui->pushButton_Apply->setEnabled(true);
    ui->pushButton_Save->setEnabled(true);
    ui->pushButton_RestoreFactory->setEnabled(true);
}

void MainWindow::clickedRestoreFactory()
{
    if (QMessageBox::question(this, tr("Confirmation"), tr("Reset all user parameters to factory values? This operation cannot be undone")) != QMessageBox::Yes) {
        return;
    }

    ui->pushButton_Apply->setEnabled(false);
    ui->pushButton_Save->setEnabled(false);
    ui->pushButton_RestoreFactory->setEnabled(false);

    restoreFactoryParams();

    ui->pushButton_Apply->setEnabled(true);
    ui->pushButton_Save->setEnabled(true);
    ui->pushButton_RestoreFactory->setEnabled(true);
}

void MainWindow::tabChanged(int index)
{
    switch (prevTab)
    {
        /* stop idle worker */
        case Tab_Info:
        {
            if (m_devParams.tcp_autodisconnect_enable)
            {
                if (workMode == Mode_TCP)
                {
                    idleWorker->disconnect();
                    idleWorker->stop();
                    QTimer::singleShot(0, idleWorker, SLOT(deleteLater()));
                    idleWorker = Q_NULLPTR;
                }
            }
        }
        break;

        /* stop image grabber */
        case Tab_Image:
        {
            imageWorker->disconnect();
            imageWorker->stop();
            QTimer::singleShot(0, imageWorker, SLOT(deleteLater()));
            imageWorker = Q_NULLPTR;
            ui->imageViewer->setImage(Q_NULLPTR);
        }
        break;

        /* stop profile grabber */
        case Tab_Profile:
        {
            profileWorker->disconnect();
            profileWorker->stop();
            QTimer::singleShot(0, profileWorker, SLOT(deleteLater()));
            profileWorker = Q_NULLPTR;
            ui->customPlot->setProfile(Q_NULLPTR);
        }
        break;

        /* stop sensors reader */
        case Tab_Sensors:
        {
            sensorsWorker->disconnect();
            sensorsWorker->stop();
            QTimer::singleShot(0, sensorsWorker, SLOT(deleteLater()));
            sensorsWorker = Q_NULLPTR;
        }
        break;
    }

    switch (index)
    {
        /* start idle worker */
        case Tab_Info:
        {
            if (workMode == Mode_TCP)
            {
                if (m_devParams.tcp_autodisconnect_enable)
                {
                    QThread *t = new QThread();
                    idleWorker = new IdleWorker();
                    idleWorker->moveToThread(t);
                    connect(idleWorker, SIGNAL(finished()), t, SLOT(terminate()));
                    connect(idleWorker, SIGNAL(destroyed(QObject*)), t, SLOT(deleteLater()));
                    t->start();
                    idleWorker->setSource(m_dev);
                    idleWorker->setTimeout(m_devParams.tcp_autodisconnect_timeout > 1 ? m_devParams.tcp_autodisconnect_timeout - 1 : 1);
                    /* scanner mode swith lag */
                    if (prevTab == Tab_Image)
                    {
                        QTimer::singleShot(500, idleWorker, SLOT(run()));
                    }
                    else
                    {
                        QTimer::singleShot(100, idleWorker, SLOT(run()));
                    }
                }
            }
        }
        break;

        /* start image grabber */
        case Tab_Image:
        {
            QThread *t = new QThread();
            imageWorker = new ImageWorker();
            imageWorker->moveToThread(t);
            connect(imageWorker, SIGNAL(updated(QImage*)), ui->imageViewer, SLOT(setImage(QImage*)));
            connect(imageWorker, SIGNAL(finished()), t, SLOT(terminate()));
            connect(imageWorker, SIGNAL(destroyed(QObject*)), t, SLOT(deleteLater()));
            connect(ui->checkBox_FlipVertical, SIGNAL(clicked(bool)), this, SLOT(toggledFlipVertical(bool)));
            t->start();
            imageWorker->setSource(m_dev);
            toggledFlipVertical(ui->checkBox_FlipVertical->isChecked());
            ui->imageViewer->setImageSize(ImageSizes(ui->comboBox_ImageSize->currentIndex()));
            QTimer::singleShot(100, imageWorker, SLOT(run()));
        }
        break;

        /* start profile grabber */
        case Tab_Profile:
        {
            QThread *t = new QThread();
            profileWorker = new ProfileWorker();
            profileWorker->moveToThread(t);
            connect(profileWorker, SIGNAL(updated(void*)), ui->customPlot, SLOT(setProfile(void*)));
            connect(profileWorker, SIGNAL(finished()), t, SLOT(terminate()));
            connect(profileWorker, SIGNAL(destroyed(QObject*)), t, SLOT(deleteLater()));
            t->start();
            profileWorker->setSource(m_dev);
            /* scanner mode swith lag */
            if (prevTab == Tab_Image)
            {
                QTimer::singleShot(500, profileWorker, SLOT(run()));
            }
            else
            {
                QTimer::singleShot(100, profileWorker, SLOT(run()));
            }
            /* resize customPlot */
            ui->customPlot->setContentsSize(m_dev->get_info().x_emr, m_dev->get_info().z_range);
        }
        break;

        /* start sensors reader */
        case Tab_Sensors:
        {
            QThread *t = new QThread();
            sensorsWorker = new SensorsWorker();
            sensorsWorker->moveToThread(t);
            connect(sensorsWorker, SIGNAL(updated(void*)), this, SLOT(updateSensors(void*)));
            connect(sensorsWorker, SIGNAL(finished()), t, SLOT(terminate()));
            connect(sensorsWorker, SIGNAL(destroyed(QObject*)), t, SLOT(deleteLater()));
            t->start();
            sensorsWorker->setSource(m_dev);
            /* scanner mode swith lag */
            if (prevTab == Tab_Image)
            {
                QTimer::singleShot(500, sensorsWorker, SLOT(run()));
            }
            else
            {
                QTimer::singleShot(100, sensorsWorker, SLOT(run()));
            }
        }
        break;
    }

    prevTab = index;
}

void MainWindow::devListSelChanged()
{
    int row = ui->tableWidget_Devices->currentRow();

    if (row >= 0)
    {
        ui->pushButton_Connect->setEnabled(true);
        ui->pushButton_Stream->setEnabled(true);

        QTimer::singleShot(0, this, SLOT(populateInfo()));
    }
    else
    {
        ui->pushButton_Connect->setEnabled(false);
        ui->pushButton_Stream->setEnabled(false);
    }
}

void MainWindow::frameRateUpdated(int freq)
{
    ui->label_FrameRate->setText(QString::number(freq));
}

void MainWindow::profileUpdated(void *ptr)
{
    rf625_profile *profile = reinterpret_cast<rf625_profile*>(ptr);

    ui->label_MeasureCount->setText(QString::number(profile->measure_cnt));
    ui->label_PacketCount->setText(QString::number(profile->packet_cnt));
    ui->label_Points->setText(QString::number(profile->points.size()));
}

void MainWindow::imageSizeChanged(int index)
{
    ui->imageViewer->setImageSize(ImageSizes(index));
}

void MainWindow::clickedSaveImage()
{
    int lastIndex = 0;
    QStringList filters;
    filters << "Image_*.png";
    QStringList files = QDir(".").entryList(filters, QDir::Files, QDir::Name);
    if (!files.isEmpty())
    {
        QStringList parts = QString(files.last()).split(QRegExp("[_\\.]"));
        if (parts.count() > 2)
        {
            lastIndex = QString(parts.at(1)).toInt();
        }
    }
    QString fileName = QString("Image_%1.png").arg(lastIndex + 1, 4, 10, QChar('0'));

    imageWorker->saveImage(fileName);
    ui->imageViewer->displayText(tr("Saved image to \"%1\"").arg(fileName), 1000);
}

void MainWindow::doubleClickedPlot(QMouseEvent *e)
{
    Q_UNUSED(e);
    if (m_dev) {
        ui->customPlot->setContentsSize(m_dev->get_info().x_emr, m_dev->get_info().z_range);
    }
}

void MainWindow::search()
{
    if (searchWorker) {
        return;
    }

    if (m_dev) {
        deviceDisconnect();
    }

    QThread *t = new QThread();
    searchWorker = new SearchWorker();
    searchWorker->moveToThread(t);
    connect(searchWorker, SIGNAL(finished()), this, SLOT(processSearchResult()));
    connect(searchWorker, SIGNAL(finished()), t, SLOT(terminate()));
    connect(searchWorker, SIGNAL(destroyed(QObject*)), t, SLOT(deleteLater()));
    t->start();

    loader->show();

    QTimer::singleShot(0, searchWorker, SLOT(run()));
}

void MainWindow::startStream()
{
    ui->tabWidget_Right->setTabEnabled(Tab_Profile, true);
    ui->tabWidget_Right->setCurrentIndex(Tab_Profile);
    ui->pushButton_Stream->setEnabled(true);
}

void MainWindow::processSearchResult()
{
    loader->hide();

    ui->pushButton_Search->setEnabled(true);

    m_devList = searchWorker->result();
    searchWorker->disconnect();
    QTimer::singleShot(0, searchWorker, SLOT(deleteLater()));
    searchWorker = Q_NULLPTR;

    if (!m_devList.empty()) {
        for(std::shared_ptr<rf625>& dev: m_devList)
        {
            QLabel *w;
            int row = ui->tableWidget_Devices->rowCount();
            ui->tableWidget_Devices->insertRow(row);
            w = new QLabel(
                        QString("%1.%2.%3.%4")
                        .arg(dev->get_info().ip_address & 0xff)
                        .arg((dev->get_info().ip_address >> 8) & 0xff)
                        .arg((dev->get_info().ip_address >> 16) & 0xff)
                        .arg((dev->get_info().ip_address >> 24) & 0xff),
                        ui->tableWidget_Devices);
            w->setAlignment(Qt::AlignCenter);
            ui->tableWidget_Devices->setCellWidget(row, 0, w);
            w = new QLabel(QString::number(dev->get_info().serial_number));
            w->setAlignment(Qt::AlignCenter);
            ui->tableWidget_Devices->setCellWidget(row, 1, w);
            w = new QLabel(
                        QString("%1:%2:%3:%4:%5:%6")
                        .arg(QString("%1").arg(dev->get_info().hardware_address[0], 2, 16, QChar('0')))
                        .arg(QString("%1").arg(dev->get_info().hardware_address[1], 2, 16, QChar('0')))
                        .arg(QString("%1").arg(dev->get_info().hardware_address[2], 2, 16, QChar('0')))
                        .arg(QString("%1").arg(dev->get_info().hardware_address[3], 2, 16, QChar('0')))
                        .arg(QString("%1").arg(dev->get_info().hardware_address[4], 2, 16, QChar('0')))
                        .arg(QString("%1").arg(dev->get_info().hardware_address[5], 2, 16, QChar('0'))),
                        ui->tableWidget_Devices);
            w->setAlignment(Qt::AlignCenter);
            ui->tableWidget_Devices->setCellWidget(row, 2, w);
            w = new QLabel(QString::number(dev->get_info().core_a_version));
            w->setAlignment(Qt::AlignCenter);
            ui->tableWidget_Devices->setCellWidget(row, 3, w);
        }
    }
    else
    {
        QMessageBox::warning(this, tr("Warning"), tr("No devices found"));
    }
}

void MainWindow::processUpgradeResult()
{
    loader->hide();

    bool ok = upgradeWorker->result();
    upgradeWorker->disconnect();
    QTimer::singleShot(0, upgradeWorker, SLOT(deleteLater()));
    upgradeWorker = Q_NULLPTR;

    ui->pushButton_Search->setEnabled(true);

    if (!ok) {
        QMessageBox::critical(this, tr("Fatal"), tr("Failed to upload firmware to scanner"));
    }
    else {
        reboot();
        QMessageBox::information(this, tr("Information"), tr("Successfully uploaded firwmare image to scanner. Rebooting scanner to install updates."));
    }
}

void MainWindow::populateParams()
{
    params->SetEntry("laser_enable", m_devParams.laser_enable);
    params->SetEntry("laser_level", int(m_devParams.laser_level));
    params->SetEntry("exposure_time", int(m_devParams.exposure_time));
    params->SetEntry("roi_top", int(m_devParams.roi_top / 32));
    params->SetEntry("roi_height", m_devParams.roi_height >= 32 ? int(m_devParams.roi_height / 32) - 1 : 1);
    params->SetEntry("trigger", int(m_devParams.trigger));
    params->SetEntry("ext_sync_divisor", int(m_devParams.ext_sync_divisor));
    params->SetEntry("ip_address", int(htonl(m_devParams.ip_address)));
    params->SetEntry("subnet_mask", int(htonl(m_devParams.subnet_mask)));
    params->SetEntry("udp_host_address", int(htonl(m_devParams.udp_host_address)));
    params->SetEntry("udp_port", int(m_devParams.udp_port));
    params->SetEntry("udp_frequency", int(m_devParams.udp_frequency));
    params->SetEntry("tcp_port", int(m_devParams.tcp_port));
    params->SetEntry("pixel_brightness_thres", int(m_devParams.pixel_brightness_thres));
    params->SetEntry("diff_brightness_thres", int(m_devParams.diff_brightness_thres));
    params->SetEntry("raw_image", m_devParams.raw_image);
    params->SetEntry("resolution", int(m_devParams.resolution));
    params->SetEntry("dhs_enable", m_devParams.dhs_enable);
    params->SetEntry("roi_enable", m_devParams.roi_enable);
    params->SetEntry("sync_input_channel", int(m_devParams.sync_input_channel));
    params->SetEntry("measure_sync_enable", m_devParams.measure_sync_enable);
    params->SetEntry("measurement_sync_input_channel", int(m_devParams.measurement_sync_input_channel));
    params->SetEntry("measurement_sync_delay", int(m_devParams.measurement_sync_delay));
    params->SetEntry("output_channel", int(m_devParams.output_channel));
    params->SetEntry("tcp_autodisconnect_enable", m_devParams.tcp_autodisconnect_enable);
    params->SetEntry("tcp_autodisconnect_timeout", int(m_devParams.tcp_autodisconnect_timeout));
    params->SetEntry("udp_stream_enable", m_devParams.udp_stream_enable);
    params->SetEntry("invert_by_x", m_devParams.invert_by_x);
    params->SetEntry("invert_by_z", m_devParams.invert_by_z);
    params->SetEntry("encoder_has_zero", m_devParams.encoder_has_zero);
    params->SetEntry("bracketing_enable", m_devParams.bracketing_enable);
    params->SetEntry("bracketing_exposure_0", int(m_devParams.bracketing_exposure_0));
    params->SetEntry("bracketing_exposure_1", int(m_devParams.bracketing_exposure_1));
    params->SetEntry("fixed_frame_enable", m_devParams.fixed_frame_enable);
}

void MainWindow::populateInfo()
{
    int row = ui->tableWidget_Devices->currentRow();
    if (row < 0)
    {
        ui->textBrowser_Info->setHtml(tr("%1<br/>\n" \
                                      "Version: %2<br/>\n" \
                                      "Build on: %3<br/>\n" \
                                      "Copyright %4<br/>\n" \
                                      "Support: <a href=\"http://%5\">%6</a><br/>\n")
            .arg(qApp->applicationDisplayName())
            .arg(qApp->applicationVersion())
            .arg(QString("%1 %2").arg(__DATE__).arg(__TIME__))
            .arg(qApp->organizationName())
            .arg(qApp->organizationDomain())
            .arg(qApp->organizationDomain()));
        return;
    }

    std::shared_ptr<rf625> dev = m_devList.at(row);
    const QString NoYes[] = {tr("No"), tr("Yes")};
    const QString Colors[] = {tr("Red"), tr("Blue"), tr("Green"), tr("Infrared")};

    QString text = tr("MAC Address:\t%1<br/>\n").arg(
        QString("%1:%2:%3:%4:%5:%6")
        .arg(QString("%1").arg(dev->get_info().hardware_address[0], 2, 16, QChar('0')))
        .arg(QString("%1").arg(dev->get_info().hardware_address[1], 2, 16, QChar('0')))
        .arg(QString("%1").arg(dev->get_info().hardware_address[2], 2, 16, QChar('0')))
        .arg(QString("%1").arg(dev->get_info().hardware_address[3], 2, 16, QChar('0')))
        .arg(QString("%1").arg(dev->get_info().hardware_address[4], 2, 16, QChar('0')))
        .arg(QString("%1").arg(dev->get_info().hardware_address[5], 2, 16, QChar('0')))
    );
    QString ip = QString("%1.%2.%3.%4")
            .arg(dev->get_info().ip_address & 0xff)
            .arg((dev->get_info().ip_address >> 8) & 0xff)
            .arg((dev->get_info().ip_address >> 16) & 0xff)
            .arg((dev->get_info().ip_address >> 24) & 0xff);
    text += tr("IP Address:\t\t%1<br/>\n").arg(ip);
    text += tr("Subnet mask:\t%1<br/>\n").arg(
        QString("%1.%2.%3.%4")
        .arg(dev->get_info().subnet_mask & 0xff)
        .arg((dev->get_info().subnet_mask >> 8) & 0xff)
        .arg((dev->get_info().subnet_mask >> 16) & 0xff)
        .arg((dev->get_info().subnet_mask >> 24) & 0xff)
    );
    text += tr("TCP port number:\t%1<br/>\n").arg(int(dev->get_info().tcp_port));
    text += tr("UDP port number:\t%1<br/>\n").arg(int(dev->get_info().udp_port));
    text += tr("DHS capable:\t%1<br/>\n").arg(NoYes[int(dev->get_info().dhs_capable)]);
    text += tr("Laser beam color:\t%1<br/>\n").arg(Colors[int(dev->get_info().laser_color)]);
    text += tr("Serial number:\t%1<br/>\n").arg(int(dev->get_info().serial_number));
    text += tr("Begin of Z range, mm:\t%1<br/>\n").arg(int(dev->get_info().z_begin));
    text += tr("Range of Z, mm:\t%1<br/>\n").arg(int(dev->get_info().z_range));
    text += tr("Range of X (SMR), mm:\t%1<br/>\n").arg(int(dev->get_info().x_smr));
    text += tr("Range of X (EMR), mm:\t%1<br/>\n").arg(int(dev->get_info().x_emr));
    text += tr("uImage version:\t%1<br/>\n").arg(int(dev->get_info().uimage_version));
    text += tr("Core A version:\t%1<br/>\n").arg(int(dev->get_info().core_a_version));
    text += tr("Core B version:\t%1<br/>\n").arg(int(dev->get_info().core_b_version));
    text += tr("FPGA version:\t%1<br/>\n").arg(int(dev->get_info().fpga_version));

    if (dev->get_info().core_a_version > 20160600)
    {
        int days = dev->get_info().uptime / 86400;
        int msec = (dev->get_info().uptime % 86400) * 1000;
        QString uptime = QTime::fromMSecsSinceStartOfDay(msec).toString();
        if (days > 0) {
            uptime.prepend(tr("%1 days, ").arg(days));
        }
        text += tr("System uptime:\t%1<br/>\n").arg(uptime);
    }

    if (dev->get_info().core_a_version >= 20170202)
    {
        text += tr("Web interface:\t<a href=\"http://%1\">http://%2</a><br/>\n").arg(ip).arg(ip);
    }

    ui->textBrowser_Info->setHtml(text);
}

void MainWindow::toggledFlipVertical(bool flip)
{
    imageWorker->toggledFlipVertical(flip);
}

static QString check_na(double val)
{
    return (val == -32.0) ? "n/a" : QString::number(val);
}
static QString check_na(int16_t val)
{
    return (val == -32000) ? "n/a" : QString::number(val);
}

void MainWindow::updateSensors(void *ptr)
{
    if (!ptr)
        return;

    rf625_sensors *sensors = reinterpret_cast<rf625_sensors*>(ptr);

    QString text;

    text += tr("CMOS sensor voltage:\t%1<br/>\n").arg(check_na(sensors->cmos_vcc));
    text += tr("CMOS sensor current:\t%1<br/>\n").arg(check_na(sensors->cmos_icc));
    text += tr("FPGA voltage:\t%1<br/>\n").arg(check_na(sensors->fpga_vcc));
    text += tr("FPGA current:\t%1<br/>\n").arg(check_na(sensors->fpga_icc));
    text += tr("Total system voltage:\t%1<br/>\n").arg(check_na(sensors->total_vcc));
    text += tr("Total system current:\t%1<br/>\n").arg(check_na(sensors->total_icc));
    text += tr("CPU voltage:\t%1<br/>\n").arg(check_na(sensors->cpu_vcc));
    text += tr("CPU current:\t%1<br/>\n").arg(check_na(sensors->cpu_icc));
    text += tr("DDR voltage:\t%1<br/>\n").arg(check_na(sensors->ddr_vcc));
    text += tr("DDR current:\t%1<br/>\n").arg(check_na(sensors->ddr_icc));
    text += tr("CPU internal temperature:\t%1<br/>\n").arg(check_na(sensors->cpu_temp_int));
    text += tr("CPU external temperature:\t%1<br/>\n").arg(check_na(sensors->cpu_temp_ext));
    text += tr("FPGA temperature:\t%1<br/>\n").arg(check_na(sensors->fpga_temp));
    text += tr("Board temperature:\t%1<br/>\n").arg(check_na(sensors->board_temp));

    ui->textBrowser_Sensors->setHtml(text);
}

void MainWindow::linkActivated(QString link)
{
    if (link == ":reboot") {
        if (QMessageBox::Yes != QMessageBox::question(this, tr("Confirmation"), tr("Reboot the scanner?"))) {
            return;
        }
        reboot();
    }
    else if (link == ":poweroff") {
        if (QMessageBox::Yes != QMessageBox::question(this, tr("Confirmation"), tr("Power off the scanner?"))) {
            return;
        }
        powerOff();
    }
    else if (link == ":upgrade") {
        if (m_dev) {
            upgrade();
        }
    }
}

bool updateDevParams(rf625_parameters& devParams)
{
    params->SaveConfig();

    rf625_parameters prev;
    memcpy(&prev, &devParams, sizeof(rf625_parameters));

    devParams.laser_enable = params->GetEntry("laser_enable").toBool();
    devParams.laser_level = params->GetEntry("laser_level").toInt();
    devParams.exposure_time = params->GetEntry("exposure_time").toInt();
    devParams.roi_top = params->GetEntry("roi_top").toInt() * 32;
    devParams.roi_height = (params->GetEntry("roi_height").toInt() + 1) * 32;
    devParams.trigger = rf625_trigger(params->GetEntry("trigger").toInt());
    devParams.ext_sync_divisor = params->GetEntry("ext_sync_divisor").toInt();
    devParams.ip_address = htonl(params->GetEntry("ip_address").toInt());
    devParams.subnet_mask = htonl(params->GetEntry("subnet_mask").toInt());
    devParams.udp_host_address = htonl(params->GetEntry("udp_host_address").toInt());
    devParams.udp_port = params->GetEntry("udp_port").toInt();
    devParams.udp_frequency = params->GetEntry("udp_frequency").toInt();
    devParams.tcp_port = params->GetEntry("tcp_port").toInt();
    devParams.pixel_brightness_thres = params->GetEntry("pixel_brightness_thres").toInt();
    devParams.diff_brightness_thres = params->GetEntry("diff_brightness_thres").toInt();
    devParams.raw_image = params->GetEntry("raw_image").toBool();
    devParams.resolution = rf625_resolution(params->GetEntry("resolution").toInt());
    devParams.dhs_enable = params->GetEntry("dhs_enable").toBool();
    devParams.roi_enable = params->GetEntry("roi_enable").toBool();
    devParams.sync_input_channel = params->GetEntry("sync_input_channel").toInt();
    devParams.measure_sync_enable = params->GetEntry("measure_sync_enable").toBool();
    devParams.measurement_sync_input_channel = params->GetEntry("measurement_sync_input_channel").toInt();
    devParams.measurement_sync_delay = params->GetEntry("measurement_sync_delay").toInt();
    devParams.output_channel = params->GetEntry("output_channel").toInt();
    devParams.tcp_autodisconnect_enable = params->GetEntry("tcp_autodisconnect_enable").toBool();
    devParams.tcp_autodisconnect_timeout = params->GetEntry("tcp_autodisconnect_timeout").toInt();
    devParams.udp_stream_enable = params->GetEntry("udp_stream_enable").toBool();
    devParams.invert_by_x = params->GetEntry("invert_by_x").toBool();
    devParams.invert_by_z = params->GetEntry("invert_by_z").toBool();
    devParams.encoder_has_zero = params->GetEntry("encoder_has_zero").toBool();
    devParams.bracketing_enable = params->GetEntry("bracketing_enable").toBool();
    devParams.bracketing_exposure_0 = params->GetEntry("bracketing_exposure_0").toInt();
    devParams.bracketing_exposure_1 = params->GetEntry("bracketing_exposure_1").toInt();
    devParams.fixed_frame_enable = params->GetEntry("fixed_frame_enable").toBool();

    return (memcmp(&prev, &devParams, sizeof(rf625_parameters)) != 0);
}

void MainWindow::applyParams()
{
    if (!updateDevParams(m_devParams)) {
        //QMessageBox::warning(this, tr("Warning"), tr("Parameters are not changed, operation aborted"));
        return;
    }

    int prevMode = workMode;
    workMode = Mode_Idle;

    if (validateParams())
    {
        if (!m_dev->write_params(m_devParams)) {
            QMessageBox::critical(this, tr("Fatal"), tr("Failed to apply parameters"));
        }
    }

    workMode = prevMode;
}

void MainWindow::saveParams()
{
    int prevMode = workMode;
    workMode = Mode_Idle;

    updateDevParams(m_devParams);
    if (validateParams())
    {
        if (!m_dev->write_params(m_devParams)) {
            QMessageBox::critical(this, tr("Fatal"), tr("Failed to apply parameters"));
        }
        if (!m_dev->flush_params()) {
            QMessageBox::critical(this, tr("Fatal"), tr("Failed to save parameters to flash"));
        }
    }

    workMode = prevMode;
}

bool MainWindow::validateParams()
{
    uint32_t res = 0;
    QString errorMessage = tr("Parameters not applied because of the following errors:\n\n");
    rf625::validate_params(m_devParams, &res);
    if (res)
    {
        if (res & ROI_Height) {
            errorMessage += tr("* Window height must be less or equal to (480 - Window Top)\n");
        }
        if (res & Subnet_Mask) {
            errorMessage += tr("* Subnet Mask is invalid\n");
        }
        else {
            if (res & IP_Address) {
                errorMessage += tr("* IP Address is invalid or cannot be used with this Subnet Mask\n");
            }
        }
        if (res & UDP_Host_Address) {
            errorMessage += tr("* UDP Host Address is invalid\n");
        }
        if (res & UDP_Port) {
            errorMessage += tr("* UDP Port cannot be set to (0) or (6001)\n");
        }
        if (res & UDP_Frequency) {
            errorMessage += tr("* UDP Frequency must be nonzero and max (250) for standard mode and (500) for DHS mode\n");
        }
        QMessageBox::critical(this, tr("Error"), errorMessage);
    }
    return (res == 0);
}

void MainWindow::restoreFactoryParams()
{
    int prevMode = workMode;
    workMode = Mode_Idle;

    if (!m_dev->reset_params()) {
        QMessageBox::critical(this, tr("Fatal"), tr("Failed to restore parameters to factory"));
    }

    workMode = prevMode;
}

void MainWindow::deviceDisconnect()
{
    int prevMode = workMode;
    workMode = Mode_Idle;

    switch (prevMode)
    {
        case Mode_TCP:
        {
            tabChanged(-1); // stop idle worker if running
            if (!m_dev->disconnect()) {
                QMessageBox::critical(this, tr("Fatal"), tr("Failed to disconnect"));
                workMode = prevMode;
                return;
            }
        }
        break;
        case Mode_UDP:
        {

        }
        break;
    }

    ui->tableWidget_Devices->setEnabled(true);
    ui->pushButton_Connect->setText(tr("Connect"));
    ui->pushButton_Search->setEnabled(true);
    ui->pushButton_Stream->setEnabled(true);
    ui->tabWidget_Left->setTabEnabled(Tab_Params, false);
    ui->tabWidget_Left->setTabEnabled(Tab_Service, false);
    ui->pushButton_Apply->setEnabled(false);
    ui->pushButton_Save->setEnabled(false);
    ui->pushButton_RestoreFactory->setEnabled(false);
    ui->tabWidget_Right->setCurrentIndex(Tab_Info);
    ui->tabWidget_Right->setTabEnabled(Tab_Image, false);
    ui->tabWidget_Right->setTabEnabled(Tab_Profile, false);
    ui->tabWidget_Right->setTabEnabled(Tab_Sensors, false);
    m_dev = Q_NULLPTR;
}

void MainWindow::reboot()
{
    if (m_dev) {
        if (!m_dev->reboot()) {
            QMessageBox::critical(this, tr("Fatal"), tr("Failed to reboot scanner"));
        }
        else {
            deviceDisconnect();
            removeDevice();
        }
    }
}

void MainWindow::powerOff()
{
    if (m_dev) {
        if (!m_dev->power_off()) {
            QMessageBox::critical(this, tr("Fatal"), tr("Failed to power off scanner"));
        }
        else {
            deviceDisconnect();
            removeDevice();
        }
    }
}

void MainWindow::upgrade()
{
    if (upgradeWorker) {
        return;
    }

    QFileDialog fd(this, QString(), QString(), "*.fw");
    fd.setAcceptMode(QFileDialog::AcceptOpen);
    fd.setFileMode(QFileDialog::ExistingFile);
    if (fd.exec() == QDialog::Accepted)
    {
        if (m_dev) {
            QThread *t = new QThread();
            upgradeWorker = new UpgradeWorker(m_dev, fd.selectedFiles().first());
            upgradeWorker->moveToThread(t);
            connect(upgradeWorker, SIGNAL(finished()), this, SLOT(processUpgradeResult()));
            connect(upgradeWorker, SIGNAL(finished()), t, SLOT(terminate()));
            connect(upgradeWorker, SIGNAL(destroyed(QObject*)), t, SLOT(deleteLater()));
            t->start();

            ui->pushButton_Search->setEnabled(false);
            ui->pushButton_Connect->setEnabled(false);
            ui->pushButton_Stream->setEnabled(false);
            ui->pushButton_Apply->setEnabled(false);
            ui->pushButton_Save->setEnabled(false);
            ui->pushButton_RestoreFactory->setEnabled(false);

            workMode = Mode_Idle;

            loader->show();

            QTimer::singleShot(0, upgradeWorker, SLOT(run()));
        }
    }
}

void MainWindow::removeDevice()
{
    Q_ASSERT(m_dev == Q_NULLPTR);
    int row = ui->tableWidget_Devices->currentRow();
    if (row >= 0)
    {
        ui->tableWidget_Devices->removeRow(row);

        QTimer::singleShot(0, this, SLOT(populateInfo()));
    }
}
