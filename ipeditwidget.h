#ifndef IPEDITWIDGET_H
#define IPEDITWIDGET_H

#include <QFrame>
#include <QLineEdit>
#include <QIntValidator>
#include "stdint.h"
#include <QHBoxLayout>
#include <QFont>
#include <QLabel>
#include <QKeyEvent>

class IPEditWidget : public QFrame
{
	Q_OBJECT

public:
	IPEditWidget(QWidget *parent = 0);
	~IPEditWidget();

	void setValue(const QString value);
	void setIP(unsigned char* ucIP, bool bReverse = false);
	QString value();
	bool getIP(unsigned char* ucIP, bool bReverse = false);

	virtual bool eventFilter( QObject *obj, QEvent *event );

public slots:
	void slotTextChanged( QLineEdit* pEdit );

signals:
	void signalTextChanged( QLineEdit* pEdit );

private:
	enum
	{
		QTUTL_IP_SIZE   = 4,
		MAX_DIGITS      = 3
	};

	QLineEdit *(m_pLineEdit[QTUTL_IP_SIZE]);
	void MoveNextLineEdit (int i);
	void MovePrevLineEdit (int i);
};

#endif // IPEDITWIDGET_H
