#include "common.h"
#include <QThread>
#include "idle.h"

static bool stopSig;

IdleWorker::IdleWorker(QObject *parent): QObject(parent)
  ,m_dev(Q_NULLPTR)
  ,m_timeout(1)
{

}

IdleWorker::~IdleWorker()
{

}

void IdleWorker::run()
{
    stopSig = false;
    bool res;
    rf625_profile *dummy = new rf625_profile;

    while (!stopSig)
    {
        QThread::sleep(m_timeout);
        if (m_dev)
        {
            res = m_dev->get_result(*dummy);
        }
    }

    delete dummy;
}

void IdleWorker::stop()
{
    stopSig = true;

    emit finished();
}
