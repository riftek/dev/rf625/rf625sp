#include "xconfig.h"
#include <math.h>
#include <QHeaderView>
#include <QFont>

// Make IPAddress_t (ok, UINT32) value from 4-byte IP address
#define MAKEIPADDR(ch0, ch1, ch2, ch3)                              \
    ((uint)(quint8)(ch3) | ((uint)(quint8)(ch2) << 8) |   \
    ((uint)(quint8)(ch1) << 16) | ((uint)(quint8)(ch0) << 24 ))
//Make IP address string from IPAddress_t representation
#define STRINGIFYIPADDR(ip) (QString("%1.%2.%3.%4").arg((ip>>24)&0xFF).arg((ip>>16)&0xFF).arg((ip>>8)&0xFF).arg((ip)&0xFF))

XConfigEntry::XConfigEntry()
{
    type = (XConfigEntryType)0;
    value = 0;
    _ctrl = NULL;
}

QString XConfigEntry::toIPAddress()
{
    return STRINGIFYIPADDR(value.toUInt());
}

XConfig::XConfig(const QString& IniFileName, const QString &IniSection, const QTableWidget *TableWidget)
{
    m_ini = new QSettings(IniFileName, QSettings::IniFormat);
    m_iniSection = IniSection;
    m_table = const_cast<QTableWidget*>(TableWidget);
}

XConfig::XConfig(QSettings *ini, const QString &IniSection, const QTableWidget *TableWidget)
{
    m_ini = ini;
    m_iniSection = IniSection;
    m_table = const_cast<QTableWidget*>(TableWidget);
}
XConfig::~XConfig()
{
    m_ini->deleteLater();
}

void XConfig::AddSpacer(const QString &title)
{
    int row = m_table->rowCount();
    m_table->insertRow(row);
    m_table->setRowHeight(row, 24);
    QLabel *label = new QLabel(title);
    QFont fnt = label->font();
    fnt.setBold(true);
    label->setFont(fnt);
    label->setStyle(m_table->horizontalHeader()->style());
    m_table->setSpan(row, 0, 1, 2);
    m_table->setCellWidget(row, 0, label);
}

void XConfig::AddEntry(const XConfigEntryType type, const QString& name, const QVariant& defaultValue, const QString& title /* = QString() */, const QString& tip /* = QString() */, const QPointF& valueConstraints /*= QPointF()*/, int floatPrecision /*= 2*/)
{
    XConfigEntry ce;
    ce.type = type;
    // read value
    int row = -1;
    if (m_table)
    {
        row = m_table->rowCount();
        m_table->insertRow(row);
        m_table->setRowHeight(row, 24);
        QLabel *label = new QLabel(title.isEmpty() ? name : title);
        if (!tip.isEmpty()) label->setToolTip(tip);
        //	label->setStyleSheet("background-color:#e9e9e9;");
        m_table->setCellWidget(row, 0, label);
    }
    switch (type)
    {
    case Int:
        {
            if (m_ini) {
                ce.value = m_ini->value(QString("%1/%2").arg(m_iniSection).arg(name), defaultValue.toInt());
            }
            if (m_table)
            {
                QSpinBox *w = new QSpinBox(m_table);
                w->setFixedWidth(100);
                w->setRange(0, INT_MAX);
                int v = ce.value.toInt();
                ce._ctrl = reinterpret_cast<QWidget*>(w);
                if (!valueConstraints.isNull())
                {
                    w->setMinimum(static_cast<int>(valueConstraints.x()));
                    w->setMaximum(static_cast<int>(valueConstraints.y()));
                }
                w->setValue(v);
                m_table->setCellWidget(row, 1, w);
            }
        }
        break;
    case Real:
        {
            if (m_ini) {
                ce.value = m_ini->value(QString("%1/%2").arg(m_iniSection).arg(name), defaultValue.toDouble());
            }
            if (m_table)
            {
                QDoubleSpinBox *w = new QDoubleSpinBox(m_table);
                w->setFixedWidth(100);
                w->setRange(INT_MIN, INT_MAX);
                double v = ce.value.toDouble();
                ce._ctrl = reinterpret_cast<QWidget*>(w);
                if (!valueConstraints.isNull())
                {
                    w->setMinimum(valueConstraints.x());
                    w->setMaximum(valueConstraints.y());
                }
                w->setDecimals(floatPrecision);
                w->setSingleStep(pow(0.1, floatPrecision));
                w->setValue(v);
                m_table->setCellWidget(row, 1, w);
            }
        }
        break;
    case IPAddress:
        {
            if (m_ini) {
                ce.value = m_ini->value(QString("%1/%2").arg(m_iniSection).arg(name), defaultValue.toUInt());
            }
            if (m_table)
            {
                IPEditWidget *w = new IPEditWidget(m_table);
                IPAddress_t ip = ce.value.toUInt();
                ce._ctrl = reinterpret_cast<QWidget*>(w);
                w->setIP((unsigned char*)&ip);
                m_table->setCellWidget(row, 1, w);
            }
        }
        break;
    case Bool:
        {
            if (m_ini) {
                ce.value = m_ini->value(QString("%1/%2").arg(m_iniSection).arg(name), defaultValue.toBool());
            }
            if (m_table)
            {
                QCheckBox *w = new QCheckBox(m_table);
                bool v = ce.value.toBool();
                ce._ctrl = reinterpret_cast<QWidget*>(w);
                w->setChecked(v);
                m_table->setCellWidget(row, 1, w);
            }
        }
        break;
    case String:
        {
            if (m_ini) {
                ce.value = m_ini->value(QString("%1/%2").arg(m_iniSection).arg(name), defaultValue.toString());
            }
            if (m_table)
            {
                QLineEdit *w = new QLineEdit(m_table);
                QString v = ce.value.toString();
                ce._ctrl = reinterpret_cast<QWidget*>(w);
                w->setText(v);
                m_table->setCellWidget(row, 1, w);
            }
        }
        break;
    case Password:
        {
            if (m_ini) {
                ce.value = m_ini->value(QString("%1/%2").arg(m_iniSection).arg(name), defaultValue.toString());
            }
            if (m_table)
            {
                QLineEdit *w = new QLineEdit(m_table);
                w->setEchoMode(QLineEdit::Password);
                QString v = ce.value.toString();
                ce._ctrl = reinterpret_cast<QWidget*>(w);
                w->setText(v);
                m_table->setCellWidget(row, 1, w);
            }
        }
        break;
    case Select:
        {
            if (m_ini) {
                ce.value = m_ini->value(QString("%1/%2").arg(m_iniSection).arg(name), QVariant());
            }
            if (m_table)
            {
                QComboBox *w = new QComboBox(m_table);
                QFontMetrics fm(w->font());
                int cw = 0;
                for (int i=0; i<defaultValue.toStringList().count(); i++) {
                    QString text(defaultValue.toStringList().at(i));
                    int tw = fm.width(text);
                    if (tw > cw)
                        cw = tw;
                    w->insertItem(i, text);
                }
                QString v = ce.value.toString();
                ce._ctrl = reinterpret_cast<QWidget*>(w);
                for (int i=0; i<w->count(); i++) {
                    if (w->itemText(i) == v) {
                        w->setCurrentIndex(i);
                        break;
                    }
                }
                w->setFixedWidth(cw + 32);
                m_table->setCellWidget(row, 1, w);
            }
        }
        break;
    }
    if (m_table) {
        if (!tip.isEmpty()) ce._ctrl->setToolTip(tip);
    }
    m_entries[name] = XConfigEntry(ce);
    // write default value if not set
    if (m_ini) {
        switch (type)
        {
        case Int:
            m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), ce.value.toInt());
            break;
        case Real:
            m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), ce.value.toDouble());
            break;
        case IPAddress:
            m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), ce.value.toUInt());
            break;
        case Bool:
            m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), ce.value.toBool());
            break;
        case String:
            m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), ce.value.toString());
            break;
        case Password:
            m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), ce.value.toString());
            break;
        }
    }
}
XConfigEntry XConfig::GetEntry(const QString name)
{
    return m_entries[name];
}
void XConfig::SetEntry(const QString& name, const QVariant& value)
{
    if (contains(name))
    {
        XConfigEntry ce = GetEntry(name);
        switch (ce.type)
        {
        case Int:
            {
                QSpinBox *w = reinterpret_cast<QSpinBox*>(m_entries[name]._ctrl);
                w->setValue(value.toInt());
            }
            break;
        case Real:
            {
                QDoubleSpinBox *w = reinterpret_cast<QDoubleSpinBox*>(m_entries[name]._ctrl);
                w->setValue(value.toDouble());
            }
            break;
        case IPAddress:	//TODO
            {
                IPEditWidget *w = reinterpret_cast<IPEditWidget*>(m_entries[name]._ctrl);
                IPAddress_t ip = value.toUInt();
                w->setIP((unsigned char*)&ip);
            }
            break;
        case Bool:
            {
                QCheckBox *w = reinterpret_cast<QCheckBox*>(m_entries[name]._ctrl);
                w->setChecked(value.toBool());
            }
            break;
        case String:
            {
                QLineEdit *w = reinterpret_cast<QLineEdit*>(m_entries[name]._ctrl);
                w->setText(value.toString());
            }
            break;
        case Password:
            {
                QLineEdit *w = reinterpret_cast<QLineEdit*>(m_entries[name]._ctrl);
                w->setText(value.toString());
            }
            break;
        case Select:    //TODO
            {
                QComboBox *w = reinterpret_cast<QComboBox*>(m_entries[name]._ctrl);
                w->setCurrentIndex(value.toInt());
            }
            break;
        }
    }
}
void XConfig::SaveConfig()
{
    foreach (QString name, m_entries.keys())
    {
        switch (m_entries[name].type)
        {
        case Int:
            {
                QSpinBox *w = reinterpret_cast<QSpinBox*>(m_entries[name]._ctrl);
                if (w) {
                    m_entries[name].value = w->value();
                    if (m_ini) {
                        m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), m_entries[name].value.toInt());
                    }
                }
            }
            break;
        case Real:
            {
                QDoubleSpinBox *w = reinterpret_cast<QDoubleSpinBox*>(m_entries[name]._ctrl);
                if (w) {
                    m_entries[name].value = w->value();
                    if (m_ini) {
                        m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), m_entries[name].value.toDouble());
                    }
                }
            }
            break;
        case IPAddress:
            {
                IPEditWidget *w = reinterpret_cast<IPEditWidget*>(m_entries[name]._ctrl);
                if (w) {
                    IPAddress_t ip;
                    w->getIP((unsigned char*)&ip);
                    m_entries[name].value = ip;
                    if (m_ini) {
                        m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), m_entries[name].value.toUInt());
                    }
                }
            }
            break;
        case Bool:
            {
                QCheckBox *w = reinterpret_cast<QCheckBox*>(m_entries[name]._ctrl);
                if (w) {
                    m_entries[name].value = w->isChecked();
                    if (m_ini) {
                        m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), m_entries[name].value.toBool());
                    }
                }
            }
            break;
        case String:
            {
                QLineEdit *w = reinterpret_cast<QLineEdit*>(m_entries[name]._ctrl);
                if (w) {
                    m_entries[name].value = w->text();
                    if (m_ini) {
                        m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), m_entries[name].value.toString());
                    }
                }
            }
            break;
        case Password:
            {
                QLineEdit *w = reinterpret_cast<QLineEdit*>(m_entries[name]._ctrl);
                if (w) {
                    m_entries[name].value = w->text();
                    if (m_ini) {
                        m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), m_entries[name].value.toString());
                    }
                }
            }
            break;
        case Select:
            {
                QComboBox *w = reinterpret_cast<QComboBox*>(m_entries[name]._ctrl);
                if (w) {
                    m_entries[name].value = w->currentIndex();
                    if (m_ini) {
                        m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(name), m_entries[name].value.toString());
                    }
                }
            }
            break;
        }
    }
}

IPAddress_t XConfig::MakeIPAddress_t(const quint8 ch0, const quint8 ch1, const quint8 ch2, const quint8 ch3)
{
    return ((uint)(ch3) | ((uint)(ch2) << 8) | ((uint)(ch1) << 16) | ((uint)(ch0) << 24 ));
}

bool XConfig::contains(const QString &key) const
{
    return m_entries.contains(key);
    //return (m_ini->contains(QString("%1/%2").arg(m_iniSection).arg(key)));
}

QVariant XConfig::value(const QString &key, const QVariant &defaultValue /*= QVariant()*/) const
{
    return (m_ini->value(QString("%1/%2").arg(m_iniSection).arg(key), defaultValue));
}

void XConfig::setValue(const QString &key, const QVariant &value)
{
    m_ini->setValue(QString("%1/%2").arg(m_iniSection).arg(key), value);
}
