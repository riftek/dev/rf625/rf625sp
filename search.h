#pragma once

#include <QObject>
#include "rf625.h"

class SearchWorker: public QObject
{
Q_OBJECT

public:
    SearchWorker(QObject *parent = Q_NULLPTR);
    inline rf625_list result() const {return m_result;}

public Q_SLOTS:
    void run();

Q_SIGNALS:
    void finished();

private:
    rf625_list m_result;
};
