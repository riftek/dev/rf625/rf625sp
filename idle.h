#pragma once

#include <QObject>
#include "rf625.h"

class IdleWorker: public QObject
{
Q_OBJECT

public:
    IdleWorker(QObject *parent = Q_NULLPTR);
    virtual ~IdleWorker();
    inline void setSource(std::shared_ptr<rf625> dev) {m_dev = dev;}
    inline void setTimeout(int seconds) {m_timeout = seconds;}
    void stop();

public Q_SLOTS:
    void run();

Q_SIGNALS:
    void updated(void*);
    void finished();

private:
    std::shared_ptr<rf625> m_dev;
    int m_timeout;
};
