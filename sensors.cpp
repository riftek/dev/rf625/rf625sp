#include "common.h"
#include <QThread>
#include "sensors.h"

static bool stopSig;

SensorsWorker::SensorsWorker(QObject *parent): QObject(parent)
  ,m_dev(Q_NULLPTR)
{
    m_sensors = new rf625_sensors;
}

SensorsWorker::~SensorsWorker()
{
    delete m_sensors;
}

void SensorsWorker::run()
{
    stopSig = false;
    bool res;

    while (!stopSig)
    {
        if (m_dev)
        {
            res = m_dev->read_sensors(*m_sensors);
            if (res)
            {
                emit updated(m_sensors);
            }
        }
        QThread::sleep(2);
    }
}

void SensorsWorker::stop()
{
    stopSig = true;

    emit finished();
}
