#pragma once

#include "common.h"
#include <QWidget>
#include <QImage>

static int displayTime = 0;

class ImageViewer : public QWidget
{
Q_OBJECT

public:
    ImageViewer(QWidget *parent = 0);

    void displayText(const QString &text, int duration = -1);

public Q_SLOTS:
    void setImage(QImage* pImage);
    void setImageSize(ImageSizes size);
    void clearText();

private:
    const QImage *m_pImage;
    ImageSizes m_size;
    QString m_text;

protected:
    void paintEvent(QPaintEvent *event);
};
