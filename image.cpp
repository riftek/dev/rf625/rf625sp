#include <QThread>
#include <QColor>
#include "image.h"

static bool stopSig;
static QString saveFileName;

ImageWorker::ImageWorker(QObject *parent): QObject(parent)
  ,m_dev(Q_NULLPTR)
  ,m_pixmap(new uint8_t[640*480])
  ,m_flipVertical(false)
{
    Q_ASSERT(m_pixmap);
}

ImageWorker::~ImageWorker()
{
    delete[] m_pixmap;
}

void ImageWorker::run()
{
    QImage img = QImage(640,480, QImage::Format_Indexed8);

    QColor beamColor = QColor("red");
    qreal	r = beamColor.redF(),
            g = beamColor.greenF(),
            b = beamColor.blueF();
    for (int c=0; c<256; c++)
    {
        QRgb col = qRgb(c*r,c*g,c*b);
        img.setColor(c, col);
    }

    saveFileName = "";

    stopSig = false;

    while (!stopSig)
    {
        if (m_dev)
        {
            if (m_dev->get_image(m_pixmap))
            {
//                memcpy(img.bits(), m_pixmap, 640*480);
                for (int y=0;y<480;y++)
                {
                    for (int x=0;x<640; x++)
                    {
                        if (!m_flipVertical)
                        {
                            img.setPixel(x,y, m_pixmap[x+y*640]);
                        }
                        else
                        {
                            img.setPixel(x,479-y, m_pixmap[x+y*640]);
                        }
                    }
                }

                if (!saveFileName.isEmpty())
                {
                    img.save(saveFileName);
                    saveFileName = "";
                }

                emit updated(&img);
            }
        }
        else
        {
            QThread::sleep(1);
        }
    }
}

void ImageWorker::saveImage(const QString &fileName)
{
    saveFileName = fileName;
}

void ImageWorker::toggledFlipVertical(bool flip)
{
    m_flipVertical = flip;
}

void ImageWorker::stop()
{
    stopSig = true;

    emit finished();
}
