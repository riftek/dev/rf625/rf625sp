#include "ajax-loader.h"

AjaxLoader::AjaxLoader(QWidget *parent): QLabel(parent)
{
    m_movie = new QMovie(":/ajax-loader.gif");
    setMovie(m_movie);

    m_movie->start();
    int w = m_movie->currentImage().size().width();
    int h = m_movie->currentImage().size().height();
    m_movie->stop();

    setFixedSize(w, h);

    if (parent)
    {
        int ww = parent->width();
        int wh = parent->height();

        move((ww-w)/2, (wh-h)/2);
    }
}

AjaxLoader::~AjaxLoader()
{
    delete m_movie;
}

void AjaxLoader::show()
{
    QLabel::show();
    m_movie->start();
}

void AjaxLoader::hide()
{
    QLabel::hide();
    m_movie->stop();
}
