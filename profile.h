#pragma once

#include <QObject>
#include "rf625.h"

#define BUFFER_COUNT (16)

class ProfileWorker: public QObject
{
Q_OBJECT

public:
    ProfileWorker(QObject *parent = Q_NULLPTR);
    virtual ~ProfileWorker();
    inline void setSource(std::shared_ptr<rf625> dev) {m_dev = dev;}
    void stop();

public Q_SLOTS:
    void run();

Q_SIGNALS:
    void updated(void*);
    void finished();

private:
    std::shared_ptr<rf625> m_dev;
    rf625_profile *m_profile[BUFFER_COUNT];
};
