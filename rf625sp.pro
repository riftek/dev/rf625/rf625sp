#-------------------------------------------------
#
# Project created by QtCreator 2018-03-19T16:06:58
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rf625sp
TEMPLATE = app

DESTDIR = $${PWD}/bin

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -L../librf625 -l$$qtLibraryTarget(rf625)

INCLUDEPATH += ../librf625/src

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        xconfig.cpp \
        ipeditwidget.cpp \
        search.cpp \
        image.cpp \
        imageviewer.cpp \
        profile.cpp \
        upgrade.cpp \
        sensors.cpp \
        idle.cpp \
        qcustomplot/qcustomplot.cpp \
        qcustomplot/qcustomplot2.cpp \
        ajax-loader.cpp

HEADERS += \
        common.h \
        mainwindow.h \
        xconfig.h \
        ipeditwidget.h \
        search.h \
        image.h \
        imageviewer.h \
        profile.h \
        upgrade.h \
        sensors.h \
        idle.h \
        qcustomplot/qcustomplot.h \
        qcustomplot/qcustomplot2.h \
        ajax-loader.h

FORMS += \
        mainwindow.ui

RESOURCES += \
        rf625sp.qrc

win32: LIBS += -lws2_32
