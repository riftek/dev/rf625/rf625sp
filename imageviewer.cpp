#include <QTimer>
#include <QPainter>
#include "imageviewer.h"

ImageViewer::ImageViewer(QWidget *parent): QWidget(parent)
  ,m_pImage(Q_NULLPTR)
  ,m_size(Size_Original)
{

}

void ImageViewer::setImage(QImage *pImage)
{
    m_pImage = pImage;
    QTimer::singleShot(0, this, SLOT(update()));
}

void ImageViewer::setImageSize(ImageSizes size)
{
    m_size = size;
}

void ImageViewer::clearText()
{
    m_text = "";
    QTimer::singleShot(0, this, SLOT(update()));
}

void ImageViewer::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter p(this);

    int w = 640, h = 480;

    switch (m_size)
    {
    case Size_Fit:
        w = width();
        h = float(h) * (float(w)/640.f);
        break;
    case Size_1_5x:
        w = float(w) * 1.5f;
        h = float(h) * 1.5f;
        break;
    case Size_2x:
        w *= 2;
        h *= 2;
        break;
    }

    if (m_pImage)
    {
        if (m_size == Size_Original)
        {
            p.drawImage(0,0, *m_pImage);
        }
        else
        {
            QImage scaledImg = m_pImage->scaled(QSize(w, h),
                Qt::KeepAspectRatio,
                Qt::SmoothTransformation);
            p.drawImage(0,0, scaledImg);
        }
    }
    else
    {
        p.fillRect(QRect(0,0, w,h), QColor("black"));
    }

    if (!m_text.isEmpty())
    {
        p.setFont(QFont("monospace", 12, 1));
        p.setPen(QColor("white"));
        p.drawText(10,22, m_text);
    }
}

void ImageViewer::displayText(const QString &text, int duration)
{
    m_text = text;
    QTimer::singleShot(0, this, SLOT(update()));
    QTimer::singleShot(duration, this, SLOT(clearText()));
}

