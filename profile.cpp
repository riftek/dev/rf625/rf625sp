#include "common.h"
#include <QThread>
#include "profile.h"

static bool stopSig;
static int nbuf = 0;
extern int workMode;

ProfileWorker::ProfileWorker(QObject *parent): QObject(parent)
  ,m_dev(Q_NULLPTR)
{
    for (int i=0; i<BUFFER_COUNT; i++)
    {
        m_profile[i] = new rf625_profile;
    }
}

ProfileWorker::~ProfileWorker()
{
    for (int i=0; i<BUFFER_COUNT; i++)
    {
        delete m_profile[i];
    }
}

void ProfileWorker::run()
{
    stopSig = false;
    nbuf = 0;
    bool res;

    while (!stopSig)
    {
        if (m_dev)
        {
            switch (workMode)
            {
                case Mode_TCP:
                    res = m_dev->get_result(*m_profile[nbuf]);
                    break;
                case Mode_UDP:
                    res = m_dev->udp_get_result(*m_profile[nbuf]);
                    break;
                default:
                    res = false;
            }
            if (res)
            {
                emit updated(m_profile[nbuf]);
                if (++nbuf == BUFFER_COUNT) {
                    nbuf = 0;
                }
            }
        }
        else
        {
            QThread::sleep(1);
        }
    }
}

void ProfileWorker::stop()
{
    stopSig = true;

    emit finished();
}
