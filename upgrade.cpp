#include "upgrade.h"

UpgradeWorker::UpgradeWorker(const std::shared_ptr<rf625> &dev, const QString &fileName, QObject *parent): QObject(parent)
  ,m_dev(dev)
  ,m_fileName(fileName)
{

}

void UpgradeWorker::run()
{
    m_result = false;

    if (!m_fileName.isEmpty())
    {
        m_result = m_dev->upgrade(m_fileName.toLocal8Bit().constData());
    }

    emit finished();
}

