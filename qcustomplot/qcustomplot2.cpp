#include "qcustomplot2.h"
#include "rf625.h"

QCPGraph *profileGraph = Q_NULLPTR;
volatile int count = 0, prevFrameTime = 0;
QElapsedTimer frameTimer;

QCustomPlot2::QCustomPlot2(QWidget *parent): QCustomPlot(parent)
  ,m_frameRate(0)
{
    setInteraction(QCP::iRangeDrag, true);
    setInteraction(QCP::iRangeZoom, true);

    profileGraph = addGraph();
    profileGraph->setName("Profile");
    profileGraph->setLineStyle(QCPGraph::lsNone);
    profileGraph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssSquare, QColor("red"), 1.0));
    profileGraph->setAdaptiveSampling(false);
    profileGraph->setPen(QPen(QColor("red")));
}

QCustomPlot2::~QCustomPlot2()
{

}

void QCustomPlot2::resizeEvent(QResizeEvent* event)
{
    if (event != nullptr)
        QCustomPlot::resizeEvent(event);
    double prevLower = yAxis->range().lower;
    yAxis->setScaleRatio(xAxis, 1.0);
    yAxis->setRangeLower(prevLower);
    emit sizeChanged(event->size());
}

QCPItemEllipse *QCustomPlot2::addEllipse(const QPointF& center, qreal xRadius, qreal yRadius, const QString& name /*= QString()*/)
{
    QCPItemEllipse* item = new QCPItemEllipse(this);
    item->topLeft->setCoords(center.x() - xRadius, center.y() - yRadius);
    item->bottomRight->setCoords(center.x() + xRadius, center.y() + yRadius);
    if (!name.isEmpty())
        item->setObjectName(name);
    return item;
}

void QCustomPlot2::setContentsSize(double width, double height)
{
    double x = xAxis->axisRect()->width(),
           y = yAxis->axisRect()->height();
    double ratioAxis = x/y,
           ratioContent = width/height;
    if (ratioAxis > ratioContent) {
        width *= ratioAxis/ratioContent;
    }
    xAxis->setRange(-width/2.0, width/2.0);
    yAxis->setRange(0, height);
}

void QCustomPlot2::setProfile(void *ptr)
{
    rf625_profile *profile = reinterpret_cast<rf625_profile*>(ptr);
    if (!count)
    {
        frameTimer.start();
        count++;
    }
    else {
        count++;
        if (frameTimer.elapsed() >= 1000) {
            m_frameRate = count - 1;
            count = 0;
            frameTimer.restart();
            prevFrameTime = 0;

            emit frameRateUpdated(m_frameRate);
        }
    }

    if (frameTimer.elapsed() - prevFrameTime >= 20) // 50Hz
    {
        prevFrameTime = frameTimer.elapsed();

        QVector<qreal> x, z;
        if (ptr)
        {
            for (int i=0; i<profile->points.size(); i++)
            {
                x.append(profile->points.at(i).x);
                z.append(profile->points.at(i).z);
            }
            emit profileUpdated(ptr);
        }
        profileGraph->setData(x, z);
        replot();
    }
}

void QCustomPlot2::mouseMoveEvent(QMouseEvent *e)
{
    emit alternateMouseMoveEvent(e);
    QCustomPlot::mouseMoveEvent(e);
}

void QCustomPlot2::mousePressEvent(QMouseEvent *e)
{
    emit alternateMousePressEvent(e);
    QCustomPlot::mousePressEvent(e);
}

void QCustomPlot2::mouseDoubleClickEvent(QMouseEvent *e)
{
    emit alternateMouseDoubleClickEvent(e);
    QCustomPlot::mouseDoubleClickEvent(e);
}
