#pragma once

#include "qcustomplot.h"
#include <QResizeEvent>
#include <QPointF>

typedef QCPItemEllipse QCPItemCircle;

class QCustomPlot2: public QCustomPlot
{
    Q_OBJECT

public:
    explicit QCustomPlot2(QWidget *parent = 0);
    virtual ~QCustomPlot2();

    QCPItemEllipse* addEllipse(const QPointF& center, qreal xRadius, qreal yRadius, const QString& name = QString());
    inline QCPItemEllipse* addEllipse(double x, double y, qreal xRadius, qreal yRadius, const QString& name = QString())
    {
        return addEllipse(QPointF(x,y), xRadius, yRadius, name);
    }
    inline QCPItemCircle* addCircle(const QPointF& center, qreal radius, const QString& name = QString())
    {
        return addEllipse(center, radius, radius, name);
    }
    inline QCPItemCircle* addCircle(double x, double y, qreal radius, const QString& name = QString())
    {
        return addEllipse(x, y, radius, radius, name);
    }

    void setContentsSize(double width, double height);

public Q_SLOTS:
    void setProfile(void*);

private:
    void resizeEvent(QResizeEvent* event);
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseDoubleClickEvent(QMouseEvent *e);

    int m_frameRate;

Q_SIGNALS:
    void sizeChanged(QSize size);
    void alternateMouseMoveEvent(QMouseEvent *e);
    void alternateMousePressEvent(QMouseEvent *e);
    void alternateMouseDoubleClickEvent(QMouseEvent *e);

    void frameRateUpdated(int);
    void profileUpdated(void*);
};
