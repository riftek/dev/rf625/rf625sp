#pragma once

#include <QObject>
#include <QString>
#include "rf625.h"

class UpgradeWorker: public QObject
{
Q_OBJECT

public:
    explicit UpgradeWorker(const std::shared_ptr<rf625>& dev, const QString& fileName, QObject *parent = Q_NULLPTR);
    inline bool result() const {return m_result;}

public Q_SLOTS:
    void run();

Q_SIGNALS:
    void finished();

private:
    std::shared_ptr<rf625> m_dev;
    QString m_fileName;
    bool m_result;
};
