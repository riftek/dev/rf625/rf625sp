#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QTableWidgetItem>
#include "rf625.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    bool m_initState;
    QSettings* m_ini;
    rf625_list m_devList;
    std::shared_ptr<rf625> m_dev;
    rf625_parameters m_devParams;

    inline void showEvent(QShowEvent *event) {Q_UNUSED(event); if (!m_initState) {m_initState=true; onCreate();}}
    inline void closeEvent(QCloseEvent *event) {Q_UNUSED(event); onClose();}
    void onCreate();
    void onClose();

    void applyParams();
    void saveParams();
    bool validateParams();
    void restoreFactoryParams();
    void deviceDisconnect();
    void reboot();
    void powerOff();
    void upgrade();
    void removeDevice();

private Q_SLOTS:
    void clickedSearch();
    void clickedConnect();
    void clickedStream();
    void clickedApply();
    void clickedSave();
    void clickedRestoreFactory();
    void tabChanged(int index);
    void devListSelChanged();
    void frameRateUpdated(int freq);
    void profileUpdated(void* ptr);
    void imageSizeChanged(int index);
    void clickedSaveImage();
    void doubleClickedPlot(QMouseEvent *e);

    void search();
    void startStream();
    void processSearchResult();
    void processUpgradeResult();
    void populateParams();
    void populateInfo();
    void toggledFlipVertical(bool flip);

    void updateSensors(void *ptr);

    void linkActivated(QString link);

Q_SIGNALS:
    void updateProfile(void*);
};

#endif // MAINWINDOW_H
