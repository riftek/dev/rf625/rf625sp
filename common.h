#pragma once

#define APP_NAME "RF625-SP"
#define APP_DISPLAY_NAME "RF625 Service Program"
#define APP_VERSION 0x010001
#define APP_REVISION 26
#define ORGANIZATION_NAME "Riftek, LLC"
#define ORGANIZATION_DOMAIN "www.riftek.com"

enum WorkMode {
    Mode_Idle,
    Mode_TCP,
    Mode_UDP
};

enum ImageSizes {
    Size_Original,
    Size_Fit,
    Size_1_5x,
    Size_2x
};
