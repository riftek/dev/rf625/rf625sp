#ifndef _XCONFIG_H_
#define _XCONFIG_H_

#include <QVariant>
#include <QSettings>
#include <QTableWidget>
#include <QLabel>
#include <QSpinBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>
#include "ipeditwidget.h"

typedef uint IPAddress_t;

typedef enum _XConfigEntryType_
{
	Int,
    Real,
	IPAddress,
	Bool,
	String,
    Password,
    Select
} XConfigEntryType;

class XConfigEntry
{
friend class XConfig;

public:

	XConfigEntry();

    inline int toInt() const {return value.toInt();}
    inline QString toString() const {return value.toString();}
    inline bool toBool() const {return value.toBool();}
    inline qreal toReal() const {return value.toReal();}
	QString toIPAddress();

private:
	XConfigEntryType		type;
	QVariant			value;
	QWidget *			_ctrl;
};

class XConfig
{
public:
    XConfig(const QString& IniFileName, const QString& IniSection, const QTableWidget *TableWidget);
    XConfig(QSettings *ini, const QString& IniSection, const QTableWidget *TableWidget);
    ~XConfig();

	QSettings *m_ini;
    QString m_iniSection;
	QTableWidget *m_table;
	QMap<QString,XConfigEntry> m_entries;

    void AddSpacer(const QString& title = QString());
    void AddEntry(const XConfigEntryType type, const QString& name, const QVariant& defaultValue, const QString& title = QString(), const QString& tip = QString(), const QPointF& valueConstraints = QPointF(), int floatPrecision = 2);
	XConfigEntry GetEntry(const QString name);
	void SetEntry(const QString& name, const QVariant& value);
	void SaveConfig();

	static IPAddress_t MakeIPAddress_t(const quint8 ch0, const quint8 ch1, const quint8 ch2, const quint8 ch3);

	// wrap for 'hidden' keys
    bool contains(const QString &key) const;
    QVariant value(const QString &key, const QVariant &defaultValue = QVariant()) const;
    void setValue(const QString &key, const QVariant &value);

private:
	Q_DISABLE_COPY(XConfig)
};

#endif
