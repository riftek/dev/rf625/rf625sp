#pragma once

#include <QObject>
#include <QImage>
#include "rf625.h"

class ImageWorker: public QObject
{
Q_OBJECT

public:
    ImageWorker(QObject *parent = Q_NULLPTR);
    virtual ~ImageWorker();
    inline void setSource(std::shared_ptr<rf625> dev) {m_dev = dev;}
    void toggledFlipVertical(bool flip);
    void stop();
    void saveImage(const QString& fileName);
    void displayText(const QString& text, int duration = -1);

public Q_SLOTS:
    void run();

Q_SIGNALS:
    void updated(QImage*);
    void finished();

private:
    std::shared_ptr<rf625> m_dev;
    uint8_t* m_pixmap;
    bool m_flipVertical;
};
